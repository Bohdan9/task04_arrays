package game.models;

import org.apache.log4j.Logger;

import java.util.Random;

public class Service {

    final static Logger logger = Logger.getLogger(Service.class);
    private Door door = new Door();
    private Random random = new Random();
    private MagicArtifact artifact = new MagicArtifact();
    private Monster monster = new Monster();
    private Hero hero = new Hero(25);
    private int monsterSum;
    private int artifactSum;
    private int monsterNumber = door.getDoorNumber() - artifact.getArtifacts();
    private int artifactNumber = door.getDoorNumber() - monsterNumber;

    public String createMonster() {
        logger.info("\nMonsters");
        for (int i = 0; i < monsterNumber; i++) {
            int monsterPoint = random.nextInt(100) + 5;
            monster.setMonsterPower(monsterPoint);
            monster.setMonsters(monsterNumber);
            monsterSum += monster.getMonsterPower();
            logger.info(" | " + monster + " | ");
        }
        logger.info(monsterSum + "sum");
        return String.valueOf(monster);
    }

    public String createArtifact() {
        logger.info("\nArtifacts");
        for (int i = 0; i < artifactNumber; i++) {
            int artifactPoints = random.nextInt(70) + 10;
            artifact.setMagicArtifactPower(artifactPoints);
            artifact.setArtifacts(artifactNumber);
            artifactSum += artifact.getMagicArtifactPower();
            logger.info(" | " + artifact + " | ");
        }
        System.out.println(artifactSum);
        return String.valueOf(artifact);
    }

    public String createBoth() {
        String both = "Monsters and artifacts";
        System.out.println(door);
        createMonster();
        createArtifact();
        return both;
    }


    public String dieHero() {
        String end = "\nGame over! Try again";
        if (monsterSum > artifactSum + hero.getHealth()) {
            System.out.println("Your hero die");
        } else {
            logger.info("You won");
        }
        return end;
    }
}
