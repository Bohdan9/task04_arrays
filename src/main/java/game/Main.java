package game;

import game.controllers.Controller;

public class Main {
    public static void main(String[] args) {
        new Controller().show();
    }
}
