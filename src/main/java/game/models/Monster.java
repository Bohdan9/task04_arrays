package game.models;

public class Monster {
    private int monsterPower;
    private int monsters;

    Monster() {
    }

    void setMonsters(int monsters) {
        this.monsters = monsters;
    }

    int getMonsterPower() {
        return monsterPower;
    }

    void setMonsterPower(int monsterPower) {
        this.monsterPower = monsterPower;
    }

    @Override
    public String toString() {
        return "Monster{" +
                "monsterPower=" + monsterPower +
                ", monsters=" + monsters +
                '}';
    }
}
