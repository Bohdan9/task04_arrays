package arrays.controller;

import arrays.models.ArrayModel;

public class ArrayController {

    public String loadData() {
        int[] arr = {1, 2, 4, 5, 6, 8, 10, 22, 15};
        int[] arr1 = {1, 2, 3, 4, 5, 6, 7, 11, 8};
        String task1 = "1: ";
        String task2 = "2:";
        String task3 = "3:";
        String view = " ";
        ArrayModel main = new ArrayModel();
        ArrayController array_controller = new ArrayController();
        int[] general = main.findSameElements(arr, arr1);
        task1 = array_controller.createTask(task1, general);
        general = main.deleteSameArrElement(arr);
        task2 = array_controller.createTask(task2, general);
        general = main.deleteSameTwoGradualArrElements(arr);
        task3 = array_controller.createTask(task3, general);
        general = main.findAnotherElement(arr, arr1);
        view += task1 + "\n" + task2 + "\n" + task3 + "\n";

        return view;
    }

    private String createTask(String task, int[] numbs) {
        StringBuilder taskBuilder = new StringBuilder(task);
        for (int numb : numbs) {
            taskBuilder.append(numb).append(" ");
        }
        task = taskBuilder.toString();
        return task;
    }
}
