package arrays.models;

import java.util.Arrays;

public class ArrayModel {


    private int[] decreaseArrayOnNumb(int[] arr, int index) {
        int[] arr1 = new int[arr.length - index];
        if (arr1.length >= 0) System.arraycopy(arr, 0, arr1, 0, arr1.length);
        return arr1;
    }

    public int[] deleteSameTwoGradualArrElements(int[] arr) {
        int[] arr1 = new int[arr.length];
        int j = 0;
        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i] != arr[i + 1]) {
                arr1[j] = arr[i];
                j++;
            }

        }
        arr1[j] = arr[arr.length - 1];
        arr1 = decreaseArrayOnNumb(arr1, arr.length - 1 - j);
        return arr1;
    }

    public int[] deleteSameArrElement(int[] arr) {
        Arrays.sort(arr);
        return deleteSameTwoGradualArrElements(arr);
    }

    public int[] findSameElements(int[] arr1, int[] arr2) {
        arr1 = deleteSameArrElement(arr1);
        arr2 = deleteSameArrElement(arr2);
        int l = 0;
        int[] outputArray = new int[arr1.length];
        for (int anArr1 : arr1) {
            for (int anArr2 : arr2) {
                if (anArr1 == anArr2) {
                    outputArray[l] = anArr1;
                    l++;
                }
            }
        }
        outputArray = decreaseArrayOnNumb(outputArray, outputArray.length - l);

        return outputArray;
    }

    public int[] findAnotherElement(int[] arr1, int[] arr2) {
        arr1 = deleteSameArrElement(arr1);
        arr2 = deleteSameArrElement(arr2);
        boolean isSame = false;
        int[] outputArray = new int[arr1.length + arr2.length];
        for (int anArr1 : arr1) {
            for (int anArr2 : arr2) {
                if (anArr1 == anArr2) {
                    isSame = true;
                }
            }
            if (!isSame) {
                System.out.println(anArr1);
            }
            isSame = false;
        }

        return outputArray;
    }
}




