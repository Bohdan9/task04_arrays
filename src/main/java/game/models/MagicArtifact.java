package game.models;

import java.util.Random;

public class MagicArtifact {
    private Random random = new Random();
    private int magicArtifactPower;
    private int artifacts;

    int getArtifacts() {
        artifacts = random.nextInt(10);
        return artifacts;
    }

    void setArtifacts(int artifacts) {
        this.artifacts = artifacts;
    }

    MagicArtifact() {
    }

    int getMagicArtifactPower() {

        return magicArtifactPower;
    }

    void setMagicArtifactPower(int magicArtifactPower) {
        this.magicArtifactPower = magicArtifactPower;
    }

    @Override
    public String toString() {
        return "MagicArtifact{" +
                "magicArtifactPower=" + magicArtifactPower +
                ", artifacts=" + artifacts +
                '}';
    }
}
