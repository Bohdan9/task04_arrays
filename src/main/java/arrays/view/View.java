package arrays.view;

import arrays.controller.ArrayController;
import org.apache.log4j.Logger;

public class View {
    final static Logger logger = Logger.getLogger(View.class);

    public static void main(String[] args) {
        logger.info("\n" + new ArrayController().loadData());
    }
}

