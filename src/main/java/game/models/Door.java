package game.models;

public class Door {
    private int doorNumber;

    Door() {
    }

    int getDoorNumber() {
        doorNumber = 10;
        return doorNumber;
    }

    @Override
    public String toString() {
        return "Door{" +
                "doorNumber=" + doorNumber +
                '}';
    }
}
