package game.view;

public class View {
    public void printGreeting(String Greeting) {
        System.out.println(Greeting);
    }

    public void printMenu(String menuee) {
        System.out.println(menuee);
    }

    public void printMonsters(String monster) {
        System.out.println(monster);
    }

    public void printArtifacts(String artifacts) {
        System.out.println(artifacts);
    }

    public void printGame(String game) {
        System.out.println(game);

    }

    public void printWinner(String winner) {
        System.out.println(winner);
    }
}
