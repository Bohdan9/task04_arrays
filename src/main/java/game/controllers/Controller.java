package game.controllers;

import game.models.Menu;
import game.models.Service;
import game.view.View;
import org.apache.log4j.Logger;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Controller {
    final static Logger logger = Logger.getLogger(Controller.class);
    private View view = new View();
    private Menu menu = new Menu();
    private Service service = new Service();
    private Scanner scanner = new Scanner(System.in);

    public void show() {
        view.printGreeting(menu.GREETING);
        while (true) {
            boolean validInput = false;
            while (!validInput) {
                view.printMenu(menu.Menu);
                try {
                    int choose = scanner.nextInt();
                    if (choose == 1) {
                        view.printMonsters(service.createMonster());
                    } else if (choose == 2) {
                        view.printArtifacts(service.createArtifact());
                    } else if (choose == 3) {
                        view.printGame(service.createBoth());
                    } else if (choose == 4) {
                        view.printWinner(service.dieHero());
                    } else if (choose == 0) {
                        System.exit(0);
                    } else {
                        logger.info("I think you miss your choice, please try again");
                    }
                } catch (InputMismatchException e) {
                    logger.error(("You have not entered a number. Try again"));
                }
            }
        }
    }

}
