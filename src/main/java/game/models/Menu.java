package game.models;

public class Menu {

    public final String GREETING = "Hi" + " " + "Let's start!";
    public final String Menu = "\n 1) Create Monsters"
            + "\n 2) Create Artifacts"
            + "\n 3) Show all rooms"
            + "\n 4) Try to win"
            + "\n 5) Press 0 to exit";
}
