package game.models;

public class Hero {
    private int health;

    int getHealth() {
        return health;
    }

    Hero(int health) {

        this.health = health;
    }

    @Override
    public String toString() {
        return "Hero{" +
                ", health=" + health +
                '}';
    }
}
